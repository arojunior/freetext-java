# Spring boot application

### Start whole application with docker
```sh
docker-compose up -d
```

### Start application locally
```sh
docker-compose up -d mysql
./mvnw spring-boot:run
```