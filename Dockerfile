FROM maven:3.5-jdk-8 as maven

COPY ./pom.xml ./pom.xml

RUN mvn dependency:go-offline -B

COPY ./src ./src

RUN mvn package -Dmaven.test.skip=true

FROM openjdk:8u171-jre-alpine

WORKDIR /upvotes

COPY --from=maven target/upvotes-*.jar ./upvotes.jar

CMD ["java", "-jar", "upvotes.jar"]