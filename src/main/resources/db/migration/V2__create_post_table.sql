CREATE TABLE post
(
    id int PRIMARY KEY AUTO_INCREMENT,
    content text,
    author_id int,
    likes int,
    dislikes int,
    created datetime
);