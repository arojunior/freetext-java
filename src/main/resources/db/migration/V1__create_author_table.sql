CREATE TABLE author
(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(200)
);