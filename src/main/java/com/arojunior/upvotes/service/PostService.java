package com.arojunior.upvotes.service;

import java.util.List;
import com.arojunior.upvotes.model.Post;

public interface PostService {
	
	List<Post> findAll();

	Post save(Post post);
}
