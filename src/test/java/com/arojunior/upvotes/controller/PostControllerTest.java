package com.arojunior.upvotes.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.arojunior.upvotes.model.Author;
import com.arojunior.upvotes.model.Post;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PostControllerTest {

	@LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;	

	@Test	
    public void postsListShouldReturnOK() throws Exception {		
		final ResponseEntity<String> posts = restTemplate.getForEntity("http://localhost:" + port + "/v1/posts", String.class);			
		assertEquals(HttpStatus.OK, posts.getStatusCode());        
    }

	@Test
    public void postsSaveShouldReturnCREATED() throws Exception {
		final Post post = new Post();
		final Author author = new Author();
		author.setId(1);
		post.setAuthor(author);
		post.setContent("Test");
		
		final ResponseEntity<String> posts = restTemplate.postForEntity("http://localhost:" + port + "/v1/posts", post, String.class);			
		assertEquals(HttpStatus.CREATED, posts.getStatusCode());        
    }	
}
